
module FunctorExercises where
class MyFunctor f where
  myfmap :: (a -> b) -> f a -> f b

--------------------------------------------------------------------------------------------------------------------------------------------
data Either' a b = Left' a | Right' b

instance Functor (Either' e) where
  fmap f (Left' x) = Left' x
  fmap f (Right' x) = Right' (f x)

--------------------------------------------------------------------------------------------------------------------------------------------
data Pair a = Pair a a

instance Functor Pair where
  fmap g (Pair x y) = Pair (g x) (g y)

-------------------------------------------------------------------------------------------------------------------------------------------
instance MyFunctor (( , ) r) where
 myfmap f (a, b) = (a, f b)

--------------------------------------------------------------------------------------------------------------------------------------------
instance MyFunctor ((->) r) where
  myfmap f g = (\x -> f ( g x))




